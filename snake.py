# Python 2
# print "Hello World"
import sys

# Python 3
print("Hello world")

myname=input("Enter your name:")
print("Hello "+myname)

print("Command line args: "+str(sys.argv))
print(sys.argv[1])

# Using files
# Reg Ex
# Loops and conditions

peopleFH=open("data.txt","r")
for person in peopleFH:
    # sys.stdout.write(person)
    #print(person.rstrip('\r\n'))
    fields=person.split(',')
    fields[-1]=fields[-1].rstrip("\r\n")
    #print("Number of elements: "+str(len(fields))+"\tLast element is: "+str(len(fields)-1))
    # if "22" in fields:
    if fields[1] == "22":
        print(type(fields[1])) # Determine the type of data stored in variable
        print("New age: "+str(int(fields[1])+1))
        print("Name: "+fields[0]+"\t"+"Phone: "+fields[-1])
    #print("Name: "+fields[0]+"\t"+"Phone: "+fields[len(fields)-1])

peopleFH.close()
