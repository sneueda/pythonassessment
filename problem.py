import sys

if len(sys.argv) < 2:
    sys.stderr.write("That's not how you do it\n")
    sys.exit(1)

print(sys.argv[1])

x=0
while x <= len(sys.argv):
    try:
        print(sys.argv[x])
        input("See something say nothing: ")
        x+=1 # x=x+1
    except IndexError:
        print("This was your typical developer issue")
        print("But we handled it :-)")
        break
    except KeyboardInterrupt:
        print("Don't do that!")
        break
    except:
        print("Some other error occurred")
        print("Cleaning up and exiting")
        break

print("Successfully reached end of the program - and coffee")
